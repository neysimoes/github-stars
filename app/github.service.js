(function() {
  'use strict';

  angular
  .module('app')
  .factory('GithubService', GithubService);

  /*@ngInject*/
  function GithubService($http) {

    var service = {
      query: function(url, config, params) {

        var req = {
          url: 'https://api.github.com' + url,
          method: 'GET'
        };
        if(params) {
          req.params = params;
        }
        angular.extend(req, config || {})

        return $http(req);
      }

    };

    return service;
  };

})();